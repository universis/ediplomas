### @universis/ediplomas

Universis api server plugin for [eDiplomas](https://ediplomas.gr/)

### Installation

    npm i @universis/ediplomas

## Configuration

Register `@universis/ediplomas` under universis api server `services` section of application configuration:
    
    {
        "services": [
            ...,
            {
                "serviceType": "@universis/ediplomas#eDiplomasService"
            }
        ]
Add the eDiplomaSchemaLoader under universis api server `schema/loaders` section of application configuration:
```
{
  ...
  "schema": {
    "loaders": [
      ...
      { "loaderType": "@universis/ediplomas#eDiplomasSchemaLoader"}
      ...
    ]
  }
  ...
}
...
```

Add service whitelist, which is are an array of remote addresses that have the right to access endpoints,
under `settings/universis/ediplomas` section of application configuration:

     "settings": {
        "universis": {
            ...
            "ediplomas": {
                "whitelist": [
                    "127.0.0.1",
                    ...
                ]
            }
        }
     }


### Usage

#### Get service version

Returns the current version of eDiplomas plugin

    curl --location --request GET 'https://universis-api/ediplomas/version'

#### Get degrees

    curl --location --request POST 'https://universis-api/ediplomas/items' \
    --header 'Content-Type: application/json' \
    --data-raw '[
        {"ssn": "12345678901"}
    ]'




 
