// tslint:disable-next-line: class-name
export declare interface eDiplomasCertificateResult {
    SSN: string;
    error: string | null;
    success: boolean;
}

// tslint:disable-next-line: class-name
export declare interface eDiplomasCertificate extends eDiplomasCertificateResult {
    certificateID?: string;
    titleID?: any;
    registrationID?: string;
    systemID?: any;
    gradeDescription?: string | null;
    gradeValue?: number;
    dateIssued?: string;
    validFrom?: string;
    status?: string;
    statusDate?: string;
    signedDocument?: string | null;
    firstNameEl?: string;
    firstNameEn?: string;
    lastNameEl?: string;
    lastNameEn?: string;
    middleNameEl?: string | null;
    middleNameEn?: string | null;
    fatherNameEl?: string;
    fatherNameEn?: string;
    motherNameEl?: string;
    motherNameEn?: string;
    birthDate?: string;
    birthYear?: any;
    gender?: number;
    citizenship?: string | null;
    SSNCOUNTRY?: string | null;
    TIN?: string | null;
    TINCOUNTRY?: string | null;
}

