import { Args, HttpBadRequestError, HttpForbiddenError, TraceUtils } from '@themost/common';
import { ExpressDataApplication } from '@themost/express';
import { NextFunction, Request, Response } from 'express';
import express from 'express';
import { ElotConverter } from '@universis/elot-converter';
import moment from 'moment';
import { eDiplomasCertificate } from './eDiplomasCertificate';
// tslint:disable-next-line: no-var-requires
const packageJson = require('../package.json');
import { GetItemsRequestItems, validate } from './GetItemsRequestSchema';

 function round(n: number, precision?: number) {
    if (typeof n !== 'number') {
        return 0;
    }
    if (precision) {
        return parseFloat(n.toFixed(precision))
    }
    return Math.round(n);
}

export function eDiplomasRouter(app: ExpressDataApplication) {
    const router  = express.Router();
    // get whitelist
    const whitelist = app.getConfiguration().getSourceAt('settings/universis/ediplomas/whitelist') || [ '127.0.0.1' ];
    if (Array.isArray(whitelist) === false) {
        throw new Error('Invalid configuration. Service whitelist must be an array');
    }
    TraceUtils.info(`Services: @universis/ediplomas is going to use the following whitelist: ${JSON.stringify(whitelist)}`);
    // get option for declared students
    const includeDeclared = app.getConfiguration().getSourceAt('settings/universis/ediplomas/include/declared');
    if (includeDeclared === true) {
        TraceUtils.info('Services: @universis/ediplomas is going to include declared students as valid student records.');
    }
    // validate whitelisted remote address
    router.use((req: Request, res: Response, next: NextFunction) => {
        // get remote address
        const remoteAddress = req.headers['x-real-ip'] || req.headers['x-forwarded-for'] || req.connection.remoteAddress;
        if (whitelist.indexOf(remoteAddress) < 0) {
            return next(new HttpForbiddenError());
        }
        return next();
    });
    // use this handler to create a router context
    router.use((req: Request, res: Response, next: NextFunction) => {
       // create router context
        const newContext = app.createContext();
        const interactiveContext = req.context;
        // finalize already assigned context
        if (interactiveContext) {
            if (typeof interactiveContext.finalize === 'function') {
                // finalize interactive context
                return interactiveContext.finalize(() => {
                    // and assign new context
                    Object.defineProperty(req, 'context', {
                        enumerable: false,
                        configurable: true,
                        get: () => {
                            return newContext
                        }
                    });
                    // exit handler
                    return next();
                });
            }
        }
        // otherwise assign context
        Object.defineProperty(req, 'context', {
            enumerable: false,
            configurable: true,
            get: () => {
                return newContext
            }
        });
        // and exit handler
        return next();
    });
    router.use((req: Request, res: Response, next: NextFunction) => {
        req.on('end', () => {
            // on end
            if (req.context) {
                // finalize data context
                return req.context.finalize( () => {
                    //
                });
            }
        });
        return next();
    });
    router.get('/version', (req: Request, res: Response) => {
        return res.json({
            version: packageJson.version
        });
    });

    router.post('/items', (req: Request, res: Response, next: NextFunction) => {
        try {
            const result = validate(req.body);
            TraceUtils.debug(`Validation: ${result}`);
            Args.check(result, new HttpBadRequestError('The payload contains invalid values.'));
            return next();
        } catch (err) {
            return next(err);
        }
    }, async (req: Request, res: Response, next: NextFunction) => {
        try {
            const values: GetItemsRequestItems = req.body;
            if (values.length === 0) {
                return res.json([]);
            }
            const items = await req.context.model('Student')
                .where('person/SSN').equal(values.map((item) => item.ssn))
                .expand({
                    name: 'person',
                    options: {
                        $expand: 'gender'
                    }
                }, {
                    name: 'department',
                    options: {
                        $select: 'id,name,alternativeCode'
                    }
                }, {
                    name: 'studyProgramSpecialty',
                    options: {
                        $expand: 'degreeTemplate'
                    }
                }, {
                    name: 'graduationGradeScale',
                    options: {
                        $expand: 'values'
                    }
                }, {
                    name: 'degreeTemplate'
                })
                .silent().getItems();
            // include declared
            if (includeDeclared == true) {
                for (let item of items) {
                    if (item.studentStatus.alternateName === 'declared') {
                        // get last declared data
                        const studentDeclaration = await req.context.model('StudentDeclaration')
                            .where('student').equal(item.id)
                            .orderByDescending('declaredDate')
                            .expand({
                                name: 'graduationGradeScale',
                                options: {
                                    $expand: 'values'
                                }
                            }).silent().getItem();
                        // if student declaration cannot be found reset status to unknown
                        // service will fail to return data for unknown user status
                        if (studentDeclaration == null) {
                            item.studentStatus = {
                                alternateName: 'invalid'
                            }
                        } else {
                            // assign student declaration attributes as graduation attributes
                            // and continue 
                            Object.assign(item, {
                                studentStatus: {
                                    alternateName: 'graduated' // treat student as graduated based on configuration settings
                                },
                                declaredDate: studentDeclaration.declaredDate,
                                graduationDate: null,
                                graduationGrade: studentDeclaration.graduationGrade,
                                graduationGradeScale: studentDeclaration.graduationGradeScale,
                                graduationGradeAdjusted: studentDeclaration.graduationGradeAdjusted,
                                graduationGradeWrittenInWords: studentDeclaration.graduationGradeWrittenInWords,
                                graduationYear: studentDeclaration.declaredYear,
                                graduationPeriod: studentDeclaration.declaredPeriod
                            });
                        }
                    }
                }
            }
            const results: eDiplomasCertificate[] = items.map((item) => {
                const valid = item.studentStatus.alternateName === 'graduated';
                if (valid === false) {
                    return {
                        SSN: item.person.SSN,
                        error: 'Student has not been graduated.',
                        success: false
                    } as eDiplomasCertificate
                }
                // get titleID
                let titleID = null;
                if (item.degreeTemplate) {
                    // get student degree template
                    titleID = item.degreeTemplate && item.degreeTemplate.titleID
                } else {
                    // get specialization degree template
                    titleID = item.studyProgramSpecialty.degreeTemplate && item.studyProgramSpecialty.degreeTemplate.titleID;
                }
                if (titleID == null) {
                    return {
                        SSN: item.person.SSN,
                        error: 'Student title cannot be found or is inaccessible.',
                        success: false
                    } as eDiplomasCertificate
                }
                // try to find gradeDescription if any
                // for future use
                let gradeDescription = null;
                if (item.graduationGradeScale != null) {
                    const graduationGradeScale: {scaleType: number, scalePrecision: number, values: Array<any>} | any = req.context.model('GradeScale').convert(item.graduationGradeScale);
                    if (graduationGradeScale.scaleType === 0) {
                        if (Array.isArray(graduationGradeScale.values)) {
                            let finalValue = item.graduationGrade;
                            if (typeof graduationGradeScale.scalePrecision === 'number') {
                                finalValue = round(finalValue, graduationGradeScale.scalePrecision);
                            }
                            const findValue = graduationGradeScale.values.find( (x: any) => {
                                return finalValue >= x.valueFrom && finalValue <= x.valueTo;
                            });
                            if (findValue) {
                                gradeDescription = findValue.alternateName;
                            }
                        }
                    }
                }

                // get valid from date
                const  validFrom = item.declaredDate || item.graduationDate;
                // get status date
                const statusDate = item.graduationDate || item.declaredDate;

                return {
                    certificateID: `${item.department.id}/${item.graduationYear.id}/${item.graduationPeriod.id}/${item.id}`,
                    titleID,
                    registrationID: `${item.department.id}/${item.studentIdentifier}`,
                    systemID: item.id,
                    gradeDescription: null, // do not use grade description (because of internal system compatibility issues)
                    gradeValue: item.graduationGrade,
                    dateIssued: item.graduationDate ? moment(item.graduationDate).format('YYYYMMDD') : null,
                    validFrom: validFrom ? moment(validFrom).format('YYYYMMDD') : null,
                    status: 'valid',
                    statusDate: statusDate ? moment(statusDate).format('YYYYMMDD') : null,
                    signedDocument: null,
                    firstNameEl: item.person.givenName,
                    firstNameEn: ElotConverter.convert(item.person.givenName),
                    lastNameEl: item.person.familyName,
                    lastNameEn: ElotConverter.convert(item.person.familyName),
                    middleNameEl: null,
                    middleNameEn: null,
                    fatherNameEl: item.person.fatherName,
                    fatherNameEn: ElotConverter.convert(item.person.fatherName),
                    motherNameEl: item.person.motherName,
                    motherNameEn: ElotConverter.convert(item.person.motherName),
                    birthDate: item.person.birthDate ? moment(item.person.birthDate).format('YYYYMMDD') : null,
                    birthYear: item.person.birthDate ? parseInt(moment(item.person.birthDate).format('YYYY'), 10) : null,
                    gender: item.person.gender.id ? item.person.gender.id : 0,
                    citizenship: null,
                    SSN: item.person.SSN,
                    SSNCOUNTRY: null,
                    TIN: item.person.vatNumber,
                    TINCOUNTRY: null,
                    error: null,
                    success: true
                } as eDiplomasCertificate;
            });
            return res.json(results);
        } catch (err) {
            return next(err);
        }
    });

    return router;
}