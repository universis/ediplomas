import { ApplicationService } from '@themost/common';
import { SchemaLoaderStrategy } from '@themost/data';
import path from 'path';

class StudyProgramSpecialtyReplacer extends ApplicationService {
    constructor(app: any) {
        super(app);
    }

    apply() {
        // get schema loader
        const schemaLoader = this.getApplication().getConfiguration().getStrategy(SchemaLoaderStrategy as new () => SchemaLoaderStrategy);
        // get model definition
        const model = schemaLoader.getModelDefinition('StudyProgramSpecialty');
        const findAttribute = model.fields.find((field: { name: string }) => {
            return field.name === 'degreeTemplate'
        });
        if (findAttribute == null) {
            model.fields.push({
                'name': 'degreeTemplate',
                'type': 'DegreeTemplate',
                'many': true,
                'multiplicity': 'ZeroOrOne',
                'mapping': {
                    'associationType': 'junction',
                    'associationAdapter': 'SpecializationDegreeTemplate',
                    'associatedObjectField': 'studyProgramSpecialty',
                    'associatedValueField': 'degreeTemplate',
                    'cascade': 'delete',
                    'parentModel': 'StudyProgramSpecialty',
                    'parentField': 'id',
                    'childModel': 'DegreeTemplate',
                    'childField': 'id'
                }
            });
            model.eventListeners = model.eventListeners || [];
            model.eventListeners.push({
                type: path.resolve(__dirname, 'listeners/OnBeforeJoinDegreeTemplate')
            });
            schemaLoader.setModelDefinition(model);
        }
    }

}

export {
    StudyProgramSpecialtyReplacer
}