// eslint-disable-next-line no-unused-vars

/**
 * @param {DataEventArgs} event 
 * @param {Function} callback 
 */
export function beforeExecute(event, callback) {
    if (event && event.emitter && event.emitter.query && event.emitter.query.$expand) {
        /**
         * @type {Array<*>}
         */
        const expand = event.emitter.query.$expand;
        if (Array.isArray(expand)) {
            // try to find if expression has a join statement with SpecializationDegreeTemplate
            let item = expand.find((item) => {
                return item.$entity && item.$entity.model === 'SpecializationDegreeTemplate';
            });

            if (item) {
                // create a new empty data object
                const testObject = event.model.convert({});
                // get degreeTemplate property which is an instance of HasParentJunction class
                const testProperty = testObject.property('degreeTemplate');
                if (testProperty == null) {
                    // do nothing and return
                    return callback();
                }
                // try to upgrade and continue
                // (this operation is important on first use)
                return testProperty.migrate((err: Error) => {
                    if (err) {
                        return callback(err);
                    }
                    if (item && item.$entity.$join == null) {
                        // set left join to ensure zero or one multiplicity
                        item.$entity.$join = 'left';
                        item = expand.find((item) => {
                            return item.$entity && item.$entity.model==='DegreeTemplate';
                        });
                        if (item && item.$entity.$join == null) {
                            item.$entity.$join = 'left';
                        }
                    }
                    return callback();
                });
            }
        }
    }
    return callback();
}
