const SocialSecuriyNumberRegEx = /^(0[1-9]|[12][0-9]|3[01])(0[1-9]|1[012])(\d\d)(\d{5,5})$/ig;
const SimpleSocialSecuriyNumberRegEx = /^(\d{11,11})$/ig;

export class SocialSecuriyNumber extends String {
    constructor(value: any) {
        super(value);
    }
    static isValid(value: string): boolean {
        return SimpleSocialSecuriyNumberRegEx.test(value);
    }
}