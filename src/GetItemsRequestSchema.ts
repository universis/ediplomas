import Ajv, {JSONSchemaType} from 'ajv';

export declare interface GetItemsRequestItem {
    ssn: string;
}

export type GetItemsRequestItems = GetItemsRequestItem[];

const GetItemsRequestSchema: JSONSchemaType<GetItemsRequestItem[]> = {
    type: 'array',
    items: {
        type: 'object',
        properties: {
            ssn: {
                type: 'string',
                nullable: false,
                pattern: '^(\\d{11,11})$'
            }
        },
        required: ['ssn'],
        additionalProperties: false
    }
};

export const validate = new Ajv().compile(GetItemsRequestSchema);